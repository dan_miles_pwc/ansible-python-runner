FROM ubuntu:latest

LABEL maintainer="daniel.miles@pwc.com"

RUN apt-get update

RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

RUN  apt-get install -y -qq apt-transport-https ca-certificates gnupg curl

RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -

RUN apt-get update

RUN apt-get install -y -qq python3 ansible google-cloud-sdk

